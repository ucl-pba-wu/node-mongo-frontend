document.addEventListener('DOMContentLoaded', function () {

    //FAB buttons init
    let fab_elems = document.querySelectorAll('.fixed-action-btn');
    //let fab_instances = M.FloatingActionButton.init(fab_elems);

    // Modals init
    let modal_elems = document.querySelectorAll('.modal');
    //let modal_instances = M.Modal.init(modal_elems);

    // Input form init
    let date_pick_elems = document.querySelectorAll('.datepicker');
    M.Datepicker.init(date_pick_elems, {
        format: 'yyyy-mm-dd',
        autoClose: 'True',
        firstDay: 1,
        defaultDate: Date.now(),
        setDefaultDate: true,
        i18n: {
            'cancel': 'Fortryd',
            'clear': 'Ryd',
            'months': ['Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'December'],
            'monthsShort': ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
            'weekdays': ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag','Fredag', 'Lørdag'],
            'weekdaysShort': ['Søn', 'Man', 'Tir', 'Ons', 'Tor', 'Fre', 'Lør'],
            'weekdaysAbbrev': ['S', 'M', 'T', 'O', 'T', 'F', 'L']
        }
    });

    // Input form select init
    let select_elems = document.querySelectorAll('select');
    let select_instances = M.FormSelect.init(select_elems);

    // Input form autocomplete init
    let auto_complete_elems = document.querySelectorAll('.autocomplete');
    let auto_complete_instances = M.Autocomplete.init(auto_complete_elems/*, {data: {"Ost": null, "Spegepølse": null}}*/);

    // input modal
    let input_modal = document.querySelector('#input_modal');
    // var input_modal = M.Modal.getInstance(elem)

    //update modal
    let update_modal = document.querySelector('#update_modal');

    //FAB add button clicked
    let fab_input_btn = document.querySelector('#fab-input-btn');
    fab_input_btn.addEventListener('click', event => {
        //console.log(event)
    });

    getTodoItems()
});


let search_input = document.getElementById('search')

search_input.addEventListener('input', search)

const api_uri = 'http://localhost:3000'

class TodoItem {
    constructor(_id, title, completed, creationDate){
        this._id = _id;
        this.title = title;
        this.completed = completed;
        this.creationDate = creationDate;
    }
}



// GET todoitems from db
async function getTodoItems(e){  
    sortOrder = document.getElementById('date_sort').getAttribute('value')  
    fetch(`${api_uri}/tasks/all/${sortOrder}`, {
        method: "GET",
        cache: "no-cache",
        headers: new Headers({
            "content-type": "application/json"
        })
    })
        .then(function (response) {
            if (response.status !== 200) {
                console.error(`Looks like there was a problem. Status code: ${response.status}`);
                return;
            }
            response.json().then(function (data) {
                updateList(data)
            });
        })
        .catch(function (error) {
            console.error("Fetch error: " + error);
        });
}

//update todo items list in UI
function updateList(data) {
    items = data
    items_list_table = document.getElementById("items_list")
    items_list_table.removeChild(document.getElementById("items_list_body"))
    items_list_table.innerHTML += '<tbody id="items_list_body"></ttbody>'
    items_list = document.getElementById("items_list_body")
    for (let i = 0; i < data.length; i++) {
        let item_id = data[i]._id//['$oid']
        items_list.innerHTML +=
            '<tr>' +
            '<td hidden>' + item_id + '</td>' +
            '<td>' + data[i].creationDate.substring(0, 10) + '</td>' +
            '<td>' + data[i].title + '</td>' +
            '<td>' + data[i].completed + '</td>' +
            '<td class="center"> <a onclick="updateModal(\'' + item_id + '\')" class="waves-effect waves-light btn-small">Rediger</a> </td>' +
            '<td class="center"> <a onclick="confirmModal(\'' + item_id + '\', \'' + data[i].title + '\', \'' + data[i].completed + '\')" class="waves-effect waves-light btn-small red accent-1">Slet</a> </td>' +
            '</tr>'
    }

    // remove the pager before adding it again - otherwise there will be duplicates
    const pager = document.getElementById("myPager");
    while (pager.lastElementChild) {
        pager.removeChild(pager.lastElementChild);
    }
    document.getElementById('total_reg').innerHTML = data.length + ' todo items fundet'

    if (data.length > 10) {
        $('#items_list').pageMe({
            pagerSelector: '#myPager',
            activeColor: 'teal lighten-1',
            prevText: 'Tilbage',
            nextText: 'Frem',
            showPrevNext: true,
            hidePageNumbers: false,
            perPage: 10
        });
    }
};

//Create a new todo item
function createTodoItem(){
    id = document.getElementById('id').value;
    item = new TodoItem(
        _id = id,
        title = document.getElementById('item_name').value,
        completed = document.getElementById('unit').value,
        creationDate = M.Datepicker.getInstance(document.getElementById('date_picker')).toString()        
        //creationDate = Date.now()
    )
    fetch(`${api_uri}/tasks/${id}`, {
        method: "PUT",
        headers: new Headers({
            "content-type": "application/json"
        }),
        body: JSON.stringify(item),
        cache: "no-cache"       
    })
        .then(function (response) {
            if (response.status !== 200) {
                console.error(`Looks like there was a problem. Status code: ${response.status}`);
                return;
            }
            getTodoItems()
        })
        .catch(function (error) {
            console.error("Fetch error: " + error);
        });

}

// runs when item delete confirmed by user
function deleteTodoItem() {
    id = document.getElementById('confirm_modal_item_id').value
    fetch(`${api_uri}/tasks/${id}`, {
        method: "DELETE",
        cache: "no-cache",
        headers: new Headers({
            "content-type": "application/json"
        })
    })
        .then(function (response) {
            if (response.status !== 200) {
                console.error(`Looks like there was a problem. Status code: ${response.status}`);
                return;
            }
            getTodoItems()
            //     //search_input.dispatchEvent(new CustomEvent('input', {search}))
        })
        .catch(function (error) {
            console.error("Fetch error: " + error);
        });
        
}

// listen for form submit button events
document.getElementById('update_form_submit').addEventListener('click', check_valid_form)
// Listen for date sort order
document.getElementById('date_sort').addEventListener('click', toggle_date_sort)

function check_valid_form (e) {
    //console.log(e)
    let form = document.getElementById('update_form')
    const instance = M.Modal.init(update_modal, {dismissible: false});
    let valid = form.reportValidity()
    if (valid === false){
        //console.table(form)
        console.error('invalid form')
        instance.open();
    }
    else {
        createTodoItem();
        instance.close();
    }
}

function toggle_date_sort(){
    console.log('clicked')
    let sort_button = document.getElementById('date_sort')
    let value = sort_button.getAttribute('value')
    let sort_button_icon = document.getElementById('date_sort_icon')
    if (sort_button_icon.innerHTML === 'arrow_drop_down'){
        sort_button_icon.innerHTML = 'arrow_drop_up'
        sort_button.value = '1'
    } else {
        sort_button_icon.innerHTML = 'arrow_drop_down'
        sort_button.value = '-1'
    }
    search_input.dispatchEvent(new CustomEvent('input', {search}))
    getTodoItems()
}

//search db, filter and refresh list
function search(e) {
    sortOrder = document.getElementById('date_sort').getAttribute('value')
    console.log(sortOrder)
    searchQuery = e.target.value
    //searchQuery = e.target.value;
    if (searchQuery.length !== 0){
        fetch(`${api_uri}/tasks/search/${searchQuery}/${sortOrder}`, {
            method: "GET",
            /*body: JSON.stringify(e.target.value),*/
            //body: JSON.stringify(search_query),
            cache: "no-cache",
            headers: new Headers({
                "content-type": "application/json"
            })
        })
            .then(function (response) {
                if (response.status !== 200) {
                    console.error(`Looks like there was a problem. Status code: ${response.status}`);
                    return;
                }
                response.json().then(async function (data) {
                    // check if anything is returned from search
                    if (data.length !== 0) {
                        updateList(data)
                        console.table(data)
                    }
                    else {
                        //nothing in search result
                    }      
                });
            })
            .catch(function (error) {
                console.error("Fetch error: " + error);
            });
    }
    else {
        getTodoItems()
    }
    

}

//inits and opens update_form
function openForm() {
    dp_label = document.getElementById('date_picker_label')
    dp_label.classList.add('active')
    document.getElementById('id').value = "111111111111"
    document.getElementById('item_name').value = ""
    document.getElementById('date_picker').value = ""
    document.getElementById('unit').value = ""
    const update_modal = document.getElementById('update_modal');
    const update_modal_instance = M.Modal.init(update_modal, {dismissible: false});
    update_modal_instance.open();
}

// gets item and populates updateModal
function updateModal(id) {
    fetch(`${api_uri}/tasks/${id}`, {
        method: "GET",
        headers: new Headers({
            "content-type": "application/json"
        })
    })
        .then(function (response) {
            if (response.status !== 200) {
                console.error(`Looks like there was a problem. Status code: ${response.status}`);
                return;
            }
            response.json().then(function (data) {
                // convertDate(data.creationDate, document.getElementById('date_picker').value)
                dp_label = document.getElementById('date_picker_label')
                dp_label.classList.add('active')
                document.getElementById('id').value = data._id
                document.getElementById('item_name').value = data.title
                document.getElementById('date_picker').value = data.creationDate
                setSelectedIndex(document.querySelector('#unit'), data.completed);
                const elem = document.getElementById('update_modal');
                const instance = M.Modal.init(elem, {dismissible: false});
                instance.open();
            });
        })
        .catch(function (error) {
            console.error("Fetch error: " + error);
        });
}

//show modal before deleting an item
function confirmModal(id, title, completed) {
    document.getElementById('confirm_modal_item_id').value = id;
    document.getElementById('confirm_modal_item_name').innerHTML = title + ' der har status ' + completed
    confirm_modal = document.getElementById('confirm_modal')
    // elem.innerHTML +=('<p hidden id="confirm_modal_item_id">'+id+'</p>')
    const instance = M.Modal.init(confirm_modal, {dismissible: false});
    instance.open();
}

//update selected item in completed dropdown (select)
function setSelectedIndex(element, value) {
    element.value = value;
    let event;
    if (typeof (Event) === 'function') {
        event = new Event('change');
    } else {  // for IE11
        event = document.createEvent('Event');
        event.initEvent('change', true, true);
    }
    element.dispatchEvent(event);
}

// function addElement(parentId, elementTag, elementId, html) {
//     // Adds an element to the document
//     var p = document.getElementById(parentId);
//     var newElement = document.createElement(elementTag);
//     newElement.setAttribute('id', elementId);
//     newElement.innerHTML = html;
//     p.appendChild(newElement);
// }

// function convertDate(old_date, picker_date){
//     var date = new Date();
//     var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(),
//                 date.getUTCDate(), date.getUTCHours(),
//                 date.getUTCMinutes(), date.getUTCSeconds());
//     console.log(picker_date)
//     console.log(old_date)
//     //console.log(new Date(now_utc));
//     console.log(now_utc);
// }
